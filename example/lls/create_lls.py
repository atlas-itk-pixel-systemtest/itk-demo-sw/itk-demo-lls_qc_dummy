from pyconfigdb import ConfigDB
from uuid import uuid4
from collections import defaultdict
from multiprocessing import Pool
from pathlib import Path
import json
import os
import logging


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(message)s", datefmt="%H:%M:%S"
)
log = logging.getLogger(__name__)


def convert_name_to_serial(chipName):
    serialPrefix = "20UPGFC"  # This will change to 20UPGFW for real wafers
    try:
        chip_number = str(int(chipName, base=16))
        # Add preceding zeros
        while len(chip_number) < 7:
            chip_number = "0" + chip_number
        return serialPrefix + str(chip_number)
    except Exception:
        msg = f"Can't convert chip name ({chipName}) into serial number, setting serial number to {chipName}"
        log.warning(msg)
        return chipName

class Config:
    # analysis_manager
    key = "demi/analysis-manager/itk-demo-configdb/api/url"
    path = Path(__file__).resolve().parent
    num_processes = 16

    files = {
        "analogscan": [
            {"file": "MeanTotMap-0.json", "name": "as_meantotmap"},
            {"file": "OccupancyMap.json", "name": "as_occupancymap"},
        ],
        "digitalscan": [{"file": "OccupancyMap.json", "name": "ds_occupancymap"}],
        "thresholdscan_hr": [
            {"file": "Chi2Map-0.json", "name": "hr_chi2map"},
            {"file": "NoiseMap-0.json", "name": "hr_noisemap"},
            {"file": "ThresholdMap-0_0_300_0_0_0.json", "name": "hr_thresholdmap"},
            {"file": "L2_warm.json.before", "name": "hr_config"},
        ],
    }

    modules = [
        {
            "serial": "20UPGM22110471",
            "frontends": ["0x1794d", "0x1795d", "0x1796d", "0x1797d"],
            "paths": [
                {
                    "type": "Jig1",
                    "scans": [
                        {
                            "type": "analogscan",
                            "path": "20UPGM22110471/Jig1/010464",
                            "short": "as",
                        },
                        {
                            "type": "digitalscan",
                            "path": "20UPGM22110471/Jig1/010463",
                            "short": "ds",
                        },
                        {
                            "type": "thresholdscan_hr",
                            "path": "20UPGM22110471/Jig1/010461",
                            "short": "hr",
                        },
                    ],
                }
            ],
        }
    ]

    lls_list = [
        {
            "author": "analysis_rk_script",
            "comment": "example LLS like structure",
            "metadata": {"id": "LLS_OB_0001", "lls_type": "Longeron", "nMods": 4},
            "name": "LLS_OB_0001",
            "type": "lls",
        }
    ]


config = Config()
configdb = ConfigDB(config.key)


def create_module(module_data: dict, lls, i: int):
    scan_tags = defaultdict(list)
    module_payloads = []
    frontend_ids = []
    first_fe = True
    for j, frontend in enumerate(module_data["frontends"]):
        payloads = []

        for scan_tag in module_data["paths"]:
            payload_ids = []
            paths = {}

            for scan in scan_tag["scans"]:
                path = f"{config.path}/{scan['path']}_std_{scan['type']}"
                paths[f"{scan['short']}_path"] = path

                for file in config.files[scan["type"]]:
                    file_name = f"{frontend}_{file['file']}"
                    file_path = os.path.join(path, file_name)
                    with open(file_path, "r") as f:
                        data = json.load(f)
                    payl_id = uuid4().hex
                    payloads.append(
                        {
                            "type": file["name"],
                            "data": json.dumps(data),
                            "name": file_name,
                            "id": payl_id,
                        }
                    )
                    payload_ids.append(payl_id)

                if first_fe:
                    file_path = os.path.join(path, "scanLog.json")
                    with open(file_path, "r") as f:
                        data = json.load(f)
                    module_payload_id = uuid4().hex
                    module_payloads.append(
                        {
                            "type": f"{scan['short']}_scanlog",
                            "data": json.dumps(data),
                            "name": "scanLog.json",
                            "id": module_payload_id,
                        }
                    )
                    payload_ids.append(module_payload_id)

            if first_fe:
                module_path_payload_id = uuid4().hex
                module_payloads.append(
                    {
                        "type": "paths",
                        "data": json.dumps(paths),
                        "id": module_path_payload_id,
                    }
                )
                payload_ids.append(module_path_payload_id)

            scan_tags[scan_tag["type"]].extend(payload_ids)

        first_fe = False
        payloads.append(
            {
                "type": "metadata",
                "data": json.dumps({"serial": convert_name_to_serial(frontend), "position": j + 1}),
                "meta": True,
            }
        )
        frontend_id = configdb.add_node(type="frontend", payloads=payloads)
        frontend_ids.append(frontend_id)
        log.info(f"{lls}: created fe {j} for module {i}.")

    module_payloads.append(
        {
            "type": "metadata",
            "data": json.dumps({"serial": module_data["serial"], "position": i + 1}),
            "meta": True,
        }
    )
    module_serial = configdb.add_node(
        type="module", children=frontend_ids, payloads=module_payloads
    )
    log.info(f"{lls}: created module: {i}.")
    return module_serial, scan_tags


def create_tags(module_serials: list[str], scan_tags: dict, rk: dict, commit: bool):
    root_id = configdb.add_node(type="modules", children=module_serials)

    payl_id = configdb.payload_create(
        type="runkey_meta", data=json.dumps(rk["metadata"]), meta=True
    )
    configdb.tag_create(
        name=rk["name"], type="runkey", objects=[root_id], payloads=[payl_id]
    )

    stage = True
    if commit:
        log.info(f"Committing lls: {rk['name']}")
        configdb.stage_commit(
            rk["name"],
            rk["name"],
            rk["type"],
            "analysis_rk_script",
            rk["comment"],
            keep_ids=True,
        )
        stage = False

    scan_tag_names = []
    for key, value in scan_tags.items():
        scan_tag_name = f"{rk['name']}_{key}"
        configdb.tag_create(
            name=scan_tag_name, type="scan-tag", payloads=value, stage=stage
        )
        scan_tag_names.append(scan_tag_name)
    configdb.tag_create(
        name=f"{rk['name']}_scan-tags",
        type="scan-tags",
        members=scan_tag_names,
        stage=stage,
    )


def create_rk(rk: dict, modules: list[dict], commit: bool):
    size = rk["metadata"]["nMods"]

    with Pool(processes=config.num_processes) as pool:
        args_list = [(modules[i % len(modules)], rk["name"], i) for i in range(size)]
        data = pool.starmap(create_module, args_list)

    # data = [create_module(modules[i % len(modules)], rk["name"], i) for i in range(size)]

    scan_tags = defaultdict(list)
    module_serials = [d[0] for d in data]
    for d in data:
        for scan_tag, payloads in d[1].items():
            scan_tags[scan_tag].extend(payloads)

    create_tags(
        module_serials=module_serials, scan_tags=scan_tags, rk=rk, commit=commit
    )

    log.info(f"Finished lls: {rk['name']}")


def create(commit: bool):
    # with Pool(processes=num_processes) as pool:
    #     args_list = [(rk, modules, commit) for rk in lls_list]
    #     data = pool.starmap(create_rk, args_list)

    for rk in config.lls_list:
        create_rk(rk, config.modules, commit)


def read_from_db():
    all_runkeys = configdb.read_all(
        table="tag", stage=False, filter="runkey", name_filter="LLS"
    )
    with open("scripts/v2/all.json", "w") as file:
        json.dump(all_runkeys, file, indent=4)
    one_runkey = configdb.tag_tree("lls_ob_0001", stage=False)
    with open("scripts/v2/one.json", "w") as file:
        json.dump(one_runkey, file, indent=4)


if __name__ == "__main__":
    create(commit=True)
    # read_from_db()
