#!/bin/bash
echo -e "Submitting analysis jobs (minimum health test)\n"
do_ana1=1
if [ $do_ana1 -eq 1 ]; then
    echo -e "Submitting call 1"
    curl -X 'POST' \
	 'http://wupp-charon.cern.ch:5112/minimum_health/?' \
	 -H 'accept: application/json' \
	 -H 'Content-Type: application/json' \
	 -d '{
     	    "ds" : "20UPGM22110471/Jig1/010463", "as_" : "20UPGM22110471/Jig1/010464", "ts" : "20UPGM22110471/Jig1/010461", "module_serial" : "20UPGM22110471", "analysis_uuid": "999999999999999", "write_results_to_db": false
	    }'
fi
do_ana2=0
if [ $do_ana2 -eq 1 ]; then 
    echo -e "\nGoing to sleep for 5 seconds"
    sleep 5
    curl -X 'POST' \
       'http://wupp-charon.cern.ch:5112/minimum_health/?' \
       -H 'accept: application/json' \
       -H 'Content-Type: application/json' \
       -d '{                                                                                                                   
          "ds" : "DQ28_Jig1/000213", "as_" : "DQ28_Jig1/000214", "ts" : "DQ28_Jig1/000215", "module_serial" : "20UPGR92110514"
       }'
fi
do_ana3=0
if [ $do_ana3 -eq 1 ]; then	
    echo -e "\nGoing to sleep for 5 seconds"
    sleep 5
    curl -X 'POST' \
       'http://wupp-charon.cern.ch:5112/minimum_health/?' \
       -H 'accept: application/json' \
       -H 'Content-Type: application/json' \
       -d '{                                                                                                                                                   
          "ds" : "DQ28_Jig2/000209", "as_" : "DQ28_Jig2/000210", "ts" : "DQ28_Jig2/000211", "module_serial" : "20UPGR92110514"
       }'
fi
do_ana4=0
if [ $do_ana4 -eq 1 ]; then	
    echo -e "\nGoing to sleep for 5 seconds"
    sleep 5
    curl -X 'POST' \
       'http://wupp-charon.cern.ch:5112/minimum_health/?' \
       -H 'accept: application/json' \
       -H 'Content-Type: application/json' \
       -d '{                                                                                                                                                   
          "ds" : "DQ28_Jig3/000203", "as_" : "DQ28_Jig3/000205", "ts" : "DQ28_Jig3/000207", "module_serial" : "20UPGR92110514"
       }'
fi
do_ana5=0
if [ $do_ana5 -eq 1 ]; then	
    echo -e "\nGoing to sleep for 5 seconds"
    sleep 5
    curl -X 'POST' \
       'http://wupp-charon.cern.ch:5112/minimum_health/?' \
       -H 'accept: application/json' \
       -H 'Content-Type: application/json' \
       -d '{                                                                                                                                                   
          "ds" : "DQ28_Jig4/000199", "as_" : "DQ28_Jig4/000200", "ts" : "DQ28_Jig4/000201", "module_serial" : "20UPGR92110514"
       }'
fi
