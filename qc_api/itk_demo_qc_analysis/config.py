from config_checker import BaseConfig, EnvSource

class QC_API_Config(BaseConfig):
    use_local_scan_data: bool = True
    sr_url: str = "http://wupp-charon.cern.ch:5111/api"
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"
    input_path: str = "/data/input"
    output_path: str = "/tmp/output"
    work_base_path: str = "/tmp/work"
    write_html: bool =  True

    CONFIG_SOURCES = [EnvSource(allow_all=True, file="../.env")]

config = QC_API_Config()
