import json
import logging
import os
import re

# import shutil
import subprocess
from datetime import datetime
from pathlib import Path

from itk_demo_qc_analysis.models import AnalysisParams, ScanInfo, Module, AnalysisInfoBase, test_type_dict
from itk_demo_qc_analysis.config import config
from pyconfigdb import ConfigDB
from pyconfigdb.exceptions import ConfigDBConnectionError as CCE
from rich import print, print_json
from rich.logging import RichHandler
from rich.rule import Rule
from module_qc_analysis_tools.cli.load_yarr_scans import main as create_scan_file_summary
from module_qc_analysis_tools.cli.globals import LogLevel

# log = logging.getLogger(__name__)
FORMAT = "%(message)s"
logging.basicConfig(
    level=logging.INFO,
    format=FORMAT,
    datefmt="[%X]",
    handlers=[RichHandler()],
)

log = logging.getLogger("rich")


MIN_HEALTH_DIR_NAME = "MIN_HEALTH_TEST"


def dummy_analog_analysis(module_serial: str, frontend_ID: str):
    print("Hello world")


def dummy_digital_analysis(module_serial: str, frontend_ID: str):
    print("Hello world")


def dummy_threshold_analysis(module_serial: str, frontend_ID: str):
    log.info("Hello world")

def create_html(clean_outls, split_lines, timestamp, testname: str):
    """Create an HTML file with links to the files in the output directory (navigator)"""
    navi_fname = f"{testname}_outputs_{timestamp}.html"
    navi_file_fullpath = Path(config.output_path) / navi_fname
    html_file = open(navi_file_fullpath, "w")
    html_file.write(f"<html>\n<head>\n<H1>{testname} Results</H1>\n")
    html_file.write("<html>\n<head>\n<H2>Navigation Page</H2>\n")
    html_file.write("Analysis date and time: <B>" + timestamp + "</B><BR><BR>\n")
    html_file.write('Back to the <A HREF="/output/Master_Navigation.html">Master Navigation Page</A>\n</head>\n')
    html_file.write("<body>\n<table>\n")
    html_file.write(f'<TR><TD><A HREF="./{testname}/{timestamp}/{testname}_stdout.txt">{testname}_stdout.txt</A></TD></TR>\n')
    html_file.write(f'<TR><TD><A HREF="./{testname}/{timestamp}/info_{testname}.json">info_{testname}.json</A></TD></TR>\n')
    for line in split_lines:
        # log.info(line)
        html_file.write(f'<TR><TD><A HREF="./{testname}/' + timestamp + "/" + line + '">' + line + "</A></TD></TR>\n")

    html_file.write("</table>\n</body>\n</html>\n")
    html_file.close()
    # subprocess.run(["cp", navi_fname, output_path])
    # shutil.cp

    # Create a master navigation file if the function is called for the first time
    path_master_navi_file = Path(config.output_path) / "Master_Navigation.html"
    navi_exists = os.path.isfile(path_master_navi_file)
    # log.info("Navigation file check = " + str(navi_exists))
    if navi_exists:
        # Add the link to the navigation file of the new test results at the top of the list:
        # log.info("Navigation file exists")
        # Read the existing file first:
        with open(path_master_navi_file, "r") as navi_f_in:
            lines_old = navi_f_in.readlines()
        # Write old lines and add new line
        with open(path_master_navi_file, "w") as navi_f_new:
            for line in lines_old:
                navi_f_new.write(line)
                if line.strip() == "<table>":
                    navi_f_new.write('<TR><TD><A HREF="./' + navi_fname + '">' + navi_fname + "</A></TD></TR>\n")

    else:
        # If the file does not exist create it:
        navi_file = open(path_master_navi_file, "w")
        navi_file.write("<html>\n<head>\n<H1>Master Navigation Page of Test Results</H1>\n")
        navi_file.write("</head>\n<body>\n<BR>\n<table>\n")
        navi_file.write('<TR><TD><A HREF="./' + navi_fname + '">' + navi_fname + "</A></TD></TR>\n")
        navi_file.write("</table>\n</body>\n</html>\n")
        navi_file.close()


def write_to_configdb(split_lines, pathminhealth, test_config: AnalysisParams):
    # Write results to config DB
    json_out_files = [d for d in split_lines if d.endswith(".json") and d.startswith("0x")]
    #log.info(json_out_files)

    serial_to_uuid = {fe.serial: fe.uuid for fe in test_config.module.frontends}


    for fname in json_out_files:
        path_outfile = pathminhealth + "/" + fname
        with open(path_outfile) as f:
            fcontent = f.read()
        fcontent = fcontent.strip("[")
        fcontent = fcontent.strip("]")
        result_dict = json.loads(fcontent)
        frontend_serial = result_dict["serialNumber"]
        # log.info(fcontent_dict)
        # This code would attach the log file of the minimum health test to the payload.
        # We decided against attaching it after some first tests:
        # log_file_name = fname.replace("json", "log")
        # log_file_path = pathminhealth + "/" + log_file_name
        # with open(log_file_path) as f_log:
        #    log_content = f_log.read()
        # fcontent_dict["log_file"] = log_content

        # log.info(result_dict)

        if test_config.write_results_to_db:
            db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
            db.payload_create(
                type="analysis_results_payload",
                data=json.dumps(result_dict),
                objects=[serial_to_uuid[frontend_serial], test_config.analysis_uuid],
                tags=[test_config.analysis_tag],
                stage=False,
            )


def download_scan_files(scan_tag: str, list_uuids: list[str], workdir: Path, scan_array: list[ScanInfo]):
    try:
        db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
    except CCE:
        log.error("ERROR: Data base is NOT accessible (health test failed)!")
        return None

    # Extract the scan data and / or the scan data locations from the LLS QC data base:
    log.debug(f"Download scan data for these front-end uuids: {list_uuids}.")
    scan_payloads = db.search_in_tag(scan_tag, payload_types=[], 
                                     object_ids=list_uuids, 
                                     payload_data=True, 
                                     order_by_object=True)

    # Loop over the differerent scans of the minimum health test:
    for scan in scan_array:
        first_result = True
        # Create directories for the scan data if they will be downloaded from the data base:
        # scandir = os.path.join(workdir, f"{scan[0]}_data")
        for object_uuid in list_uuids:
            if object_uuid not in scan_payloads:
                log.error(f"ERROR: No scan data found for {scan.name} for frontend {object_uuid}, workdir: {workdir}!")
            else:
                object_payloads = scan_payloads[object_uuid]
                for key in object_payloads:
                    if (scan.db_prefix) in key:
                        if first_result:
                            scandir = workdir / f"{scan.name}_data"
                            log.debug(f"scandir = {scandir}")
                            log.debug(f"Working directory is {os.getcwd()}")
                            scandir.mkdir()
                            scan.path_on_disk = scandir
                            # os.chdir(scandir)
                            first_result = False

                        file_name = object_payloads[key]["name"]
                        with open(scandir / file_name, "w") as outf:
                            outf.write(object_payloads[key]["data"])


def get_paths(scan_tag: str, module_uuid: str, scan_array: list[ScanInfo]):
    try:
        db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
    except CCE:
        log.error("ERROR: Data base is NOT accessible (health test failed)!")
        return None

    module_payloads = db.search_in_tag(scan_tag, payload_types=["paths"], 
                                       object_ids=[module_uuid], 
                                       payload_data=True, 
                                       order_by_object=True)
    paths = json.loads(module_payloads[module_uuid]["paths"]["data"])
    for scan in scan_array:
        scan.path_on_disk = Path(paths[f"{scan.db_prefix}path"])


def scan_analysis(ana_request: AnalysisParams):
    module = ana_request.module

    # Instiantiate a AnalysisInfo object   
    test_info: AnalysisInfoBase = test_type_dict[ana_request.analysis_type]()

    # Get time stamp of start of the script:
    time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M%S_%f")
    # log.info("Time stamp = " + time_stamp)
    log.info(f"Performing a {ana_request.analysis_type} for the module at the position {module.mod_pos_on_lls}.")

    # Create a working directory:
    workdir_name = test_info.workdir_prefix + "_" + module.serial_number + "_" + time_stamp
    workdir = Path(config.work_base_path) / f"{workdir_name}"
    workdir.mkdir(parents=True)

    # Record time stamps of the execution of the minimum health test
    with open(Path(config.output_path) / test_info.time_stamp_filename, "a") as ts_file:
        ts_file.write(time_stamp + "\n")

    # Create an array with the scan information:
    scan_info_array = test_info.get_scan_info_array()

    # Prepare the steering (info) file of the locations of scan outputs and other meta information.
    # output_of_process = subprocess.check_output(["ls", "-l"], stderr=subprocess.STDOUT)
    # The scan_array is provided via a call by reference.
    uuid_list = [fe.uuid for fe in module.frontends]
    uuid_list.append(module.uuid)

    if config.use_local_scan_data:
        get_paths(ana_request.scan_tag, module.uuid, scan_info_array)
    else:
        download_scan_files(ana_request.scan_tag, uuid_list, workdir, scan_info_array)
    log.debug(scan_info_array)

    create_scan_file_summary(
        output_yarr=str(workdir),
        module_sn=module.serial_number,
        verbosity=LogLevel.info,
        test_name=test_info.module_qc_test_name,
        digitalscan=test_info.scan_list.digitalscan.path_on_disk,
        analogscan=test_info.scan_list.analogscan.path_on_disk,
        thresholdscan_hr=test_info.scan_list.thresholdscan_hr.path_on_disk,
        thresholdscan_hd=test_info.scan_list.thresholdscan_hd.path_on_disk,
        noisescan=test_info.scan_list.noisescan.path_on_disk,
        totscan=test_info.scan_list.totscan.path_on_disk,
        thresholdscan_zerobias=test_info.scan_list.thresholdscan_zerobias.path_on_disk,
        discbumpscan=test_info.scan_list.discbumpscan.path_on_disk,
        mergedbumpscan=test_info.scan_list.mergedbumpscan.path_on_disk,
        sourcescan=test_info.scan_list.sourcescan.path_on_disk
    )

    # Execute the minimum health test:
    print(Rule(test_info.test_executable_name))
    output_of_process = subprocess.check_output(
        [test_info.test_executable_name, "-i", f"{str(workdir)}/info_{test_info.module_qc_test_name}.json", "--output-dir", str(workdir)],
        stderr=subprocess.STDOUT,
        text=True,
    )
    # log.info(output_of_process)
    with open(f"{str(workdir)}/{test_info.module_qc_test_name}_stdout.txt", "w") as file:
        file.write(str(output_of_process))

    # Get the directory name (date and time) of the output of the current analysis:
    outls = subprocess.check_output(["ls", "-1", f"{str(workdir)}/{test_info.module_qc_test_name}"], text=True)
    log.debug(outls)
    clean_outls = outls.strip()

    # Create a list of the output files created by the mininum health test:
    results_path = f"{str(workdir)}/{test_info.module_qc_test_name}/" + clean_outls
    # log.debug(pathminhealth)
    list_of_out_files = subprocess.check_output(["ls", "-1", results_path], text=True)
    # log.debug(list_of_out_files)
    split_lines = list_of_out_files.split("\n")
    # Remove the last element of the list which is '' and refers to the mother directory:
    split_lines.pop()
    log.debug(f"List of output files: {split_lines}")

    # Create an export directory:
    # export_path = output_path / f"MIN_HEALTH_TEST/{clean_outls}/"
    export_path = Path(config.output_path) / f"{test_info.module_qc_test_name}/{time_stamp}/"
    export_path.mkdir(parents=True, exist_ok=True)
    log.info(f"export_path = {export_path}")

    # Copy the file with the stdout of the minimum health test to the export output directory
    subprocess.run(["cp", f"{str(workdir)}/{test_info.module_qc_test_name}_stdout.txt", export_path])

    # Copy the steering file of the minimum health test to the export output directory
    subprocess.run(["cp", f"{str(workdir)}/info_{test_info.module_qc_test_name}.json", export_path])

    # Copy the output files of the minimum health test:
    # subprocess.run(["cp", "-r", f"outputs/MIN_HEALTH_TEST/{clean_outls}/*", export_path])
    for outf in split_lines:
        subprocess.run(["cp", f"{str(workdir)}/{test_info.module_qc_test_name}/{clean_outls}/{outf}", export_path])

    if config.write_html:
        create_html(clean_outls, split_lines, time_stamp, test_info.module_qc_test_name)

    write_to_configdb(split_lines, results_path, ana_request)

    return None

    # Clean up: remove the working directory
    # os.chdir("..")

    # content_workdir = os.listdir(workdir_name)
    # log.debug(content_workdir)


if __name__ == "__main__":  #
    # minimum_health_test("20UPGM22110466")
    # minimum_health_test("20UPGM22110466")

    ap = AnalysisParams(
        **{
            "as_": "20UPGM22110471/Jig1/010464",
            "ts": "20UPGM22110471/Jig1/010461",
            "module_serial": "20UPGM22110471",
            "analysis_uuid": "999999999999999",
            "write_results_to_db": False,
        }
    )

    scan_analysis(ap)
    scan_analysis(ap)
    scan_analysis(ap)
