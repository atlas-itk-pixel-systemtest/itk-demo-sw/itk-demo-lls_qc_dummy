import json
import re
from dataclasses import dataclass, field
from types import SimpleNamespace
from typing import ClassVar, Optional
from unittest.mock import Base

from pydantic import BaseModel, Field
from rich import print
from rich.columns import Columns
from rich.pretty import pprint
from rich.table import Table

from pydantic import BaseModel


class FrontendID(BaseModel):
    uuid: str
    serial: str
class Module(BaseModel):
    uuid: str
    serial_number: str
    mod_pos_on_lls: int
    frontends: list[FrontendID]

class AnalysisParams(BaseModel):
    module: Module
    analysis_type: str
    analysis_uuid: str
    analysis_tag: str
    scan_tag: str
    write_results_to_db: bool


class MinimumHealthTestFEResults(BaseModel):
    property_: Optional[dict] = Field(None, alias="property")  # ClassVar[dict] = {"ANALYSIS_VERSION": "2.2.6"}
    Metadata: Optional[dict] = None  # = Field(default_factory=dict) #ClassVar[dict] = {"QC_LAYER": "L2"}
    MIN_HEALTH_THRESHOLD_MEAN: float
    MIN_HEALTH_THRESHOLD_SIGMA: float
    MIN_HEALTH_NOISE_MEAN: float
    MIN_HEALTH_NOISE_SIGMA: float
    MIN_HEALTH_TOT_MEAN: float
    MIN_HEALTH_TOT_SIGMA: float
    MIN_HEALTH_DEAD_DIGITAL: float
    MIN_HEALTH_BAD_DIGITAL: float
    MIN_HEALTH_DEAD_ANALOG: float
    MIN_HEALTH_BAD_ANALOG: float
    MIN_HEALTH_THRESHOLD_FAILED_FITS: float
    MIN_HEALTH_HIGH_ENC: float
    MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT: float
    MIN_HEALTH_HIGH_ENC_INDEPENDENT: float


class MinimumHealthTestFE(BaseModel):
    serialNumber: str
    testType: str
    passed: bool
    results: MinimumHealthTestFEResults


class MinimumHealthTestResults(BaseModel):
    minimum_health_test_results: list[MinimumHealthTestFE]


class ScanInfo(BaseModel):
    name: str = ""
    db_prefix: str = ""
    path_on_disk: str = ""


if __name__ == "__main__":
    with open("output/dump_mht_20UPGM22110471.json") as f:
        mht_json = json.load(
            f,
            # object_hook=lambda d: SimpleNamespace(**d),
            # object_hook=lambda d: MinimumHealthTestResults(**d),
        )
    # pprint(mht_json, expand_all=True)
    mht = MinimumHealthTestResults(**mht_json)
    print(mht)

    fet = []
    for fe in mht.minimum_health_test_results:
        results = {k: f"{v:7.2f}" for k, v in vars(fe.results).items() if k.startswith("MIN")}

        table = Table(
            "Result",
            "Value",
            title=fe.serialNumber,
        )
        for k, v in results.items():
            table.add_row(k, v)
        fet.append(table)

    print(Columns(fet))
