import httpx
from pyconfigdb import ConfigDB
from pyconfigdb.exceptions import ConfigDBResponseError as CRE


def qc_container_health_test(url: str):
    rt_val = False
    # Do a health test of the QC container before submiting the requests:
    res = httpx.get(url, follow_redirects=True)
    if res.status_code == 200:
        rt_val = True
    else:
        rt_val = False
    return rt_val


# Return value: True if the tag was created, False if the tag already exists
def create_tag(lls_db: ConfigDB, **kwargs):
    try:
        lls_db.tag_create(**kwargs)
    except CRE as e:
        # check if error is caused by a duplicate tag
        if e.status == 464:
            return False
        else:
            raise e
    return True


# TODO: bad and dead analog pixels are not independent
upper_limit = {
    "MIN_HEALTH_DEAD_ANALOG": 153,
    "MIN_HEALTH_BAD_ANALOG": 153,
    "MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT": 1536,
    "MIN_HEALTH_HIGH_ENC_INDEPENDENT": 1536,
    "PIXEL_FAILURE_ELECTRICALLY_FAILED": 154,
    "PIXEL_FAILURE_DISCONNECTED_PIXELS": 600,
    "PIXEL_FAILURE_FAILING_PIXELS": 600,
}


def BAI_test(MIN_HEALTH_DEAD_ANALOG: int, MIN_HEALTH_BAD_ANALOG: int):
    # Bad analog integrated (BAI) test:
    return 0 <= (MIN_HEALTH_DEAD_ANALOG + MIN_HEALTH_BAD_ANALOG) <= upper_limit["MIN_HEALTH_DEAD_ANALOG"]


def TFF_test(MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT: int):
    # Threshold failed fits independent (TFF) test:
    return 0 <= MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT <= upper_limit["MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT"]


def HEN_test(MIN_HEALTH_HIGH_ENC_INDEPENDENT: int):
    # High ENC independent test:
    return 0 <= MIN_HEALTH_HIGH_ENC_INDEPENDENT <= upper_limit["MIN_HEALTH_HIGH_ENC_INDEPENDENT"]


def EF_test(PIXEL_FAILURE_ELECTRICALLY_FAILED: int):
    # ELECTRICALLY_FAILED test:
    return 0 <= PIXEL_FAILURE_ELECTRICALLY_FAILED <= upper_limit["PIXEL_FAILURE_ELECTRICALLY_FAILED"]


def DP_test(PIXEL_FAILURE_DISCONNECTED_PIXELS: int):
    # DISCONNECTED_PIXELS test:
    return 0 <= PIXEL_FAILURE_DISCONNECTED_PIXELS <= upper_limit["PIXEL_FAILURE_DISCONNECTED_PIXELS"]


def FP_test(PIXEL_FAILURE_FAILING_PIXELS: int):
    # FAILING_PIXELS test:
    return 0 <= PIXEL_FAILURE_FAILING_PIXELS <= upper_limit["PIXEL_FAILURE_FAILING_PIXELS"]


# def check_upper_limit(limit: int, value: int):
#     rt_value = False
#     if (value < limit):
#         rt_value = True
#     return rt_value
