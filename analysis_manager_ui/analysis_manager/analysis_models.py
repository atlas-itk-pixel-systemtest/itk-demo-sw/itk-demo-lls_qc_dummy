import logging
from abc import ABC
from analysis_manager.models import FrontendResult, TestVariable
from analysis_manager.tools import BAI_test, HEN_test, TFF_test, EF_test, DP_test, FP_test
from pydantic import BaseModel

log_sars = logging.getLogger("Analysis_Manager_Logger")

class AnalysisFeatures(ABC, BaseModel):
    analysis_label: str = "analysis"
    testtype: str = "none"
    results: list[FrontendResult] = []

    def run_test(self):
        test_values = self.tests_to_dict()
        loc_results: list[FrontendResult] = []

        for testtype, values in test_values.items():
            func = test_types[testtype]
            color = "red"
            if func(**values):
                color = "lime"
            loc_results.append(FrontendResult(color=color, acronym=testtype))    

        return loc_results

    def tests_to_dict(self):
        return {result.acronym: {variable.name: variable.value for variable in result.variables} for result in self.results}

class MinimumHealthTestFeatures(AnalysisFeatures):
    analysis_label: str = "Minimum Health Test"
    testtype: str = "MIN_HEALTH_TEST"

    def __init__(self):
        super().__init__()
        self.results = []

        bai_result = FrontendResult(acronym="BAI")
        bai_result.variables.append(TestVariable(name="MIN_HEALTH_DEAD_ANALOG", value=0, description="Number of dead analog pixels"))
        bai_result.variables.append(TestVariable(name="MIN_HEALTH_BAD_ANALOG", value=0, description="Number of bad analog pixels"))

        tff_result = FrontendResult(acronym="TFF")
        tff_result.variables.append(TestVariable(name="MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT", value=0, description="Number of threshold failed fits"))

        hen_result = FrontendResult(acronym="HEN")
        hen_result.variables.append(TestVariable(name="MIN_HEALTH_HIGH_ENC_INDEPENDENT", value=0, description="Number of high ENC pixels"))

        self.results.append(bai_result)
        self.results.append(tff_result)
        self.results.append(hen_result)


class PixelFailureAnalysisFeatures(AnalysisFeatures):
    analysis_label: str = "Pixel Failure Analysis"
    testtype: str = "PIXEL_FAILURE_ANALYSIS"

    def __init__(self):
        super().__init__()
        self.results = []

        ef_result = FrontendResult(acronym="EF")
        ef_result.variables.append(TestVariable(name="PIXEL_FAILURE_ELECTRICALLY_FAILED", value=0))

        dp_result = FrontendResult(acronym="DP")
        dp_result.variables.append(TestVariable(name="PIXEL_FAILURE_DISCONNECTED_PIXELS", value=0))

        fp_result = FrontendResult(acronym="FP")
        fp_result.variables.append(TestVariable(name="PIXEL_FAILURE_FAILING_PIXELS", value=0))

        self.results.append(ef_result)
        self.results.append(dp_result)
        self.results.append(fp_result)


analysis_types = {"MIN_HEALTH_TEST": MinimumHealthTestFeatures, "PIXEL_FAILURE_ANALYSIS": PixelFailureAnalysisFeatures}
test_types = {"BAI": BAI_test, "TFF": TFF_test, "HEN": HEN_test, "EF": EF_test, "DP": DP_test, "FP": FP_test}