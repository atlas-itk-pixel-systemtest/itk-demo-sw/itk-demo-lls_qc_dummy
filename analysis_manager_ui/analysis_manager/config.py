from config_checker import BaseConfig, EnvSource
import socket

hostname = socket.gethostname()
hostname = hostname.split(".")[0]


list_analysis_types = ["Minimum Health Test", "Pixel Failure Analysis"]

class QC_API_Config(BaseConfig):
    sr_url: str = f"http://{hostname}:5111/api"
    qc_api_key: str = f"demi/{hostname}/itk-demo-qc-analysis/api/url"
    configdb_key: str = f"demi/{hostname}/itk-demo-configdb/api/url"
    pdb_api_key: str = f"demi/{hostname}/itk-demo-pdb/api/url"
    runkey_ui_key: str = f"demi/{hostname}/itk-demo-configdb/runkey-ui/url"
    use_db: bool = True
    institution: str = "CERN"

    CONFIG_SOURCES = [EnvSource(allow_all=True, file=".env")]

config = QC_API_Config()