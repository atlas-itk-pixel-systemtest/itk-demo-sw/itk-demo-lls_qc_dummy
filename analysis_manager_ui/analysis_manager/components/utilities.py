import reflex as rx

content_box_style = dict(
    background_color=rx.color("blue", 4),
    border_radius="10px",
    width="100%",
    padding="10px",
    spacing="2"
)

def error_box_loading() -> rx.Component:
    return rx.box(
        rx.heading(
            "LLS information not loaded!\n\n\n\n",
            color="red",
            white_space="pre",
        ),
        style=content_box_style
    )
