import reflex as rx
from analysis_manager.states.analysis_manager_state import AnalysisManagerState
from analysis_manager.components.utilities import error_box_loading
from analysis_manager.components.utilities import content_box_style
from analysis_manager.config import list_analysis_types
from analysis_manager.models import ModuleInfo, scan_data_status_flags
from analysis_manager.states.lls_state import LLSState

def composer_panel() -> rx.Component:
    return rx.flex(
        rx.flex(
            rx.heading("Scan Analysis Composition", size="6"),
            rx.cond(
                ((AnalysisManagerState.n_selected_mods > 0) &
                 (LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)),
                rx.flex(
                    rx.input(
                        list="analysis_tags_datalist",
                        placeholder="Add analysis-tag",
                        on_blur=AnalysisManagerState.set_analysis_tag,
                    ),
                    rx.html(LLSState.analysis_tags_datalist),
                    rx.button("Submit", on_click=AnalysisManagerState.submit_analysis, loading=AnalysisManagerState.submit_loading_state),
                    spacing="2",
                ),
                rx.text(""),
            ),
            justify="between",
        ),
        rx.cond(
            ((LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)
                & (LLSState.selected_LLS_ID != "none")
                & (LLSState.nMods_of_LLS > 0)
            ),
            rx.flex(
                operation_row(),
                rx.flex(
                    rx.foreach(LLSState.mod_info, mod_box),
                        flex_wrap="wrap",
                        spacing="2",
                ),
                style=content_box_style,
                direction="column"
            ),
            error_box_loading()
        ),
        rx.cond(
            (AnalysisManagerState.show_composer_legend & 
                (LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)
            ),
            rx.flex(
                rx.text("Legend", weight="bold"),
                rx.text("Abbreviations of available scan data: ", 
                        rx.text.strong("d"), 
                        "= digital scan, ",
                        rx.text.strong("a"),
                        "= analog scan, ",
                        rx.text.strong("t"),
                        " = threshold scan"),
                rx.flex(
                    rx.text("Colour coding: ", white_space="pre"),
                    rx.text("green", color="green"),
                    rx.text(" = scan data are available with the correct tag, ", white_space="pre"),
                    rx.text("red", color="red"),
                    rx.text(" = scan data are not available", white_space="pre")
                ),
                style=content_box_style,
                direction="column"
            )
        ),
        spacing="3",
        direction="column",
    )

def operation_row() -> rx.Component:
    return rx.flex(
        rx.input(
            list="scan_tags_datalist",
            label="Select a scan tag",
            placeholder=AnalysisManagerState.select_scan_placeholder,
            on_blur=lambda value: AnalysisManagerState.load_scan_info(value),
            width="20%",
                ),
        rx.html(LLSState.scan_tags_datalist),
        rx.cond(
                (~AnalysisManagerState.scan_tag_check),
                rx.text("Please enter valid scan tag.",color_scheme="red")
            ),
#        rx.button(
#            "Load tagged scan information", 
#            on_click=AnalysisManagerState.load_scan_info
#        ),
        rx.cond(
            (AnalysisManagerState.select_all_mods_state == "select"),
            rx.button(
                "Select all",
                on_click=AnalysisManagerState.select_all_mods
            ),
            rx.button(
                "De-select all",
                on_click=AnalysisManagerState.select_all_mods,
                background_color="silver",
                color="dimgray"
            ),
        ),
        rx.select(
            list_analysis_types,
            label = "Select an analysis type",
            default_value = list_analysis_types[0],
            on_change=lambda value: AnalysisManagerState.set_analysis_type_and_reload_scan_tag_info(value)            
        ),
        rx.cond(
            ~AnalysisManagerState.show_composer_legend,  
            rx.button(
                "Show legend",
                on_click=AnalysisManagerState.toggle_composer_legend
            ),
            rx.button(
                "Hide legend",
                on_click=AnalysisManagerState.toggle_composer_legend,
                background_color="silver",
                color="dimgray"           
            )
        ),    
        spacing="2",
        padding_bottom="10px"
    )

def mod_box(mod_array: ModuleInfo) -> rx.Component:
    posStr = f"Mod. {mod_array.position:2d}"
    return rx.card(
        rx.flex(
            rx.box(
                rx.text(posStr, weight="bold", text_align="center", white_space="pre"),
                rx.text(
                    mod_array.module_serial,
                    size="1",
                    #font_size="x-small",
                    color_scheme="gray",
                ),
            ),
            rx.box(
                rx.flex(
                    rx.cond((
                        (mod_array.scans_dict["digitalscan"].status == scan_data_status_flags["GOOD"]) | 
                        (mod_array.scans_dict["analogscan"].status == scan_data_status_flags["GOOD"])
                        ),
                        rx.checkbox(
                            on_change=AnalysisManagerState.toggle(mod_array.position), 
                            checked=mod_array.checked_for_analysis
                        ),
                        rx.text("")
                    ),
                    rx.stack(
                        rx.foreach(
                            mod_array.scans_dict.values()[:3],
                            lambda scan: scan_data_box(scan.status, scan.short),
                        ),
                        flex_direction="row",
                        spacing="1"
                    ),
                    justify="between",
                    padding="6px",
                ),
                rx.flex(
                    rx.stack(
                        rx.foreach(
                            mod_array.scans_dict.values()[3:],
                            lambda scan: scan_data_box(scan.status, scan.short),
                        ),
                        flex_direction="row",
                        spacing="1"
                    ),
                    justify="between",
                    padding="6px",
                ),
            ),
            spacing="3",
            direction="column",
        ),
        margin="2px",
        padding="6px",
        border_radius="10px",
        border_width="1px",
        background_color=rx.color("gray", 4),
        text_align="center",
    )

def scan_data_box(data_availability_flag: int, test_symbol: str) -> rx.Component:
    return rx.match(
        data_availability_flag,
        (0, rx.box(test_symbol, background_color="silver", padding_left="4px", padding_right="4px", font_size="1")),
        (1, rx.box(test_symbol, background_color="lime", padding_left="4px", padding_right="4px")),
        (2, rx.box(test_symbol, background_color="darkseagreen", padding_left="4px", padding_right="4px")),
        (3, rx.box(test_symbol, background_color="gold", padding_left="4px", padding_right="4px")),
        (4, rx.box(test_symbol, background_color="red", padding_left="4px", padding_right="4px"))
    )
