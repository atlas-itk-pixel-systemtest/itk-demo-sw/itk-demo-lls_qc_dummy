import reflex as rx
import logging
from analysis_manager.config import config
from analysis_manager.states.lls_state import LLSState

log_lls = logging.getLogger("Analysis_Manager_Logger")

def LLS_panel() -> rx.Component:
    return rx.card(
        rx.flex(
            rx.heading("LLS Selection", size="6"),
            rx.flex(
                rx.cond(
                    config.use_db,
                    rx.text(
                        "Local LLS data base: ",
                        rx.link(
                            f"{LLSState.config_db_url}",
                            href=f"{LLSState.config_db_url}",
                            is_external=True,
                        ),
                    ),
                    rx.text("Access mode: File"),
                ),
                justify="between",
            ),
            rx.flex(
                rx.button("Get LLS Info", on_click=LLSState.getLLS_Info),
                rx.cond(
                    LLSState.got_LLS_info,
                    rx.box(
                        "Received",
                        background_color="lime",
                        border_radius="8px",
                        padding="4px",
                        width="20%",
                        text_align="center",
                    ),
                    rx.box(
                        "No info",
                        background_color="silver",
                        color="white",
                        border_radius="8px",
                        padding="4px",
                        width="20%",
                        text_align="center",
                    ),
                ),
                rx.cond(
                    LLSState.got_LLS_info,
                    rx.box(
                        f"{LLSState.nLLS} LLS available",
                        padding="4px",
                        text_align="left",
                    ),
                    rx.box(""),
                ),
                spacing="6",
                justify="between",
                flex_wrap="wrap",
            ),
            rx.cond(
                LLSState.found_duplicate_LLS,
                rx.text(
                    "WARNING: There are duplicate LLS IDs!!!",
                    color="red",
                    weight="bold",
                    size="4",
                ),
                rx.text(""),
            ),
            rx.box(
                rx.input(
                    list="lls_ids_datalist",
                    label="Select an LLS ID",
                    placeholder=LLSState.select_placeholder,
                    on_blur=LLSState.check_lls_id_input,
                    width="70%",
                ),
                rx.html(LLSState.lls_ids_datalist),
                spacing="6",
            ),
            rx.cond(
                (~LLSState.lls_id_check),
                rx.text("Please enter valid ID.", color_scheme="red"),
            ),
            rx.flex(
                rx.cond(
                    (LLSState.selected_LLS_ID == "none"),
                    rx.text(""),
                    rx.text(
                        f"{LLSState.selected_LLS_ID}",
                        weight="bold",
                        size="1",
                    ),
                ),
                rx.cond(
                    (
                        (LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)
                        & (LLSState.selected_LLS_ID != "none")
                    ),
                    rx.text(
                        f"{LLSState.type_selected}",
                        f"     N modules = {LLSState.nMods_of_LLS}",
                        weight="bold",
                        size="1",
                        white_space="pre",
                    ),
                    rx.text(""),
                ),
                spacing="6",
                justify="between",
                flex_wrap="wrap",
            ),
            rx.button(
                "Load LLS properties",
                on_click=LLSState.load_prop_selected,
                width="70%",
            ),
            spacing="3",
            direction="column",
        ),
        width="100%",
        min_height="25vh",
    )
