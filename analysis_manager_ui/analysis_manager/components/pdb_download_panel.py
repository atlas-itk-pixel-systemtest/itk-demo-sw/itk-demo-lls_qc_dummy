import reflex as rx
from analysis_manager.states.pdb_download_state import PDBDownloadState

def list_LLS_serials_foreach(list_LLS_serials: str):
    return rx.list.item(list_LLS_serials)

def pdb_download() -> rx.Component:
    return rx.box(
        rx.vstack(
            rx.box(
                rx.heading("Production Database Download", align="left", size="8"),
                rx.heading(
                    "Download information on loaded-local support (LLS) structures from the ITk production database (PDB)",
                    size="4",
                    margin_y="7px",
                ),
                rx.button("Get LLS Info", on_click=PDBDownloadState.getLLSInfo),

                rx.hstack(
                    rx.input(
                        placeholder="componentSerial",
                        on_change = PDBDownloadState.set_component_serial,
                    ),
                    rx.input(
                        placeholder="runkey",
                        on_change = PDBDownloadState.set_runkey,
                    ),
                    rx.button("Submit", on_click=PDBDownloadState.get_pdb_component, loading=PDBDownloadState.get_pdb_component_loading),
                ),
                rx.text(PDBDownloadState.result, color_scheme="red"),
                rx.cond(
                    PDBDownloadState.get_pdb_component_status,
                    rx.box(
                        rx.link("runkey-ui", href=f"{PDBDownloadState.runkey_ui_url}/?path=editor&name={PDBDownloadState.runkey}", is_external=True),
                    ),
                ),
                margin="10px"
            ),
                rx.grid(
                    rx.box(
                        rx.heading("LLS Info", size="4"),
                        rx.list.unordered(
                        rx.foreach(PDBDownloadState.list_LLS_serials, list_LLS_serials_foreach)
                        ),
                    ),
                    rx.box(
                        rx.heading("LLS", size="4"),
                        ),
                    ),
                ),
        
        background_color=rx.color("gray", 3),
        width="100%",
        height="100%"
        ),