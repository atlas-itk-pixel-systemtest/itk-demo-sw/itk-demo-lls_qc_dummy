import reflex as rx
from analysis_manager.states.lls_state import LLSState
from analysis_manager.states.results_state import ScanAnalysisResultsState
from analysis_manager.components.utilities import error_box_loading
from analysis_manager.components.utilities import content_box_style
from analysis_manager.models import FrontendResult, ModuleInfo, Frontend, SummaryHistogram

def results_panel() -> rx.Component:
    return rx.flex(
        rx.heading(f"{ScanAnalysisResultsState.analysis_label} Results", size="6"),
        rx.cond(
            ((LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)
                & (LLSState.selected_LLS_ID != "none")
                & (LLSState.nMods_of_LLS > 0)
            ),
            rx.cond(
                (LLSState.n_analysis_tags > 0),
                rx.flex(
                    select_analysis_tag_flex(),
                    rx.cond(
                        (ScanAnalysisResultsState.loaded_analysis_tag == ScanAnalysisResultsState.selected_analysis_tag),
                        rx.cond(
                            (ScanAnalysisResultsState.results_type == "Summary"), 
                            module_summary(),
                            histogram_view()
                        ),
                        rx.box(
                            rx.heading(
                                "Reload the analysis tag information.",
                                color="red"
                            ),
                            background_color=rx.color("blue", 4),
                            border_radius="10px",
                            padding="10px",
                            width="100%"
                        )
                    ),
                    rx.cond(
                        ScanAnalysisResultsState.show_results_legend,
                        legend(),
                        rx.text("")
                    ),
                    spacing="2",
                    direction="column"
                ),
                rx.box(
                    rx.heading(
                            f"There are no analysis tags for LLS {LLSState.selected_LLS_ID}.\n\n\n\n",
                            color="red",
                            white_space="pre",
                    ),
                    background_color=rx.color("blue", 4),
                    border_radius="10px",
                    padding="10px",
                    width="100%",
                )
            ),
            error_box_loading()
        ),
        spacing="2",
        direction="column"
    )
#         rx.select(
#             LLSState.list_analysis_tags,
#             placeholder=ScanAnalysisResultsState.analysis_tag_list_placeholder,
#             on_change=ScanAnalysisResultsState.set_selected_analysis_tag, 

def select_analysis_tag_flex() -> rx.Component:
    return  rx.flex(
        rx.input(
            list="analysis_tags_datalist",
            placeholder=ScanAnalysisResultsState.analysis_tag_list_placeholder,
            on_blur=ScanAnalysisResultsState.check_analysis_tag_input, 
            label="Select an analysis tag",
            width="25%"
        ),
        rx.html(LLSState.analysis_tags_datalist),
        rx.cond(
            (~ScanAnalysisResultsState.analysis_tag_check),
            rx.text("Please enter valid analysis tag.", color_scheme="red"),
        ),
        rx.button(
            "Load the tagged analysis results", 
            on_click=ScanAnalysisResultsState.load_tagged_ana_results
        ),
        rx.cond(
            ScanAnalysisResultsState.results_type == "Summary",
            rx.button(
                "Show Histograms",
                on_click=ScanAnalysisResultsState.toggle_results_type,
                # background_color="silver",
                # color="dimgray"    
            ),
            rx.button(
                "Show Summary",
                on_click=ScanAnalysisResultsState.toggle_results_type    
            )
        ),
        rx.cond(
            ~ScanAnalysisResultsState.show_results_legend,  
            rx.button(
                "Show legend",
                on_click=ScanAnalysisResultsState.toggle_results_legend
            ),
            rx.button(
                "Hide legend",
                on_click=ScanAnalysisResultsState.toggle_results_legend,
                background_color="silver",
                color="dimgray"           
            )
        ),    
        spacing="2"
    )

def module_summary() -> rx.Component:
    return  rx.flex(
        rx.foreach(LLSState.mod_info, mod_result_box),
        flex_wrap="wrap",
        spacing="1",
        background_color=rx.color("blue", 4),
        border_radius="10px",
        padding="5px",
    )

def mod_result_box(mod_array: ModuleInfo) -> rx.Component:
    return rx.card(
        rx.flex(
            rx.box(
                rx.text(
                    f"M {mod_array.position:02d}", 
                    weight="bold", 
                    text_align="center",
                    color=mod_array.label_color, 
                    white_space="pre"),
                rx.text(mod_array.module_serial, font_size="x-small", color=mod_array.serialnumber_color),
            ),
            rx.box(
                rx.grid(
                    rx.foreach(mod_array.frontends, front_end_box),
                    columns="2",
                    spacing="1"
                ),
            ),
            spacing="2",
            direction="column"
        ),
        margin="1px",
        padding="4px",
        border_radius="8px",
        border_width="1px",
        #background_color="silver",
        text_align="center"
    )

#fe_pos: int, lls_pos: int) -> rx.Component:
def front_end_box(fe_list: Frontend) -> rx.Component:
    return rx.flex(
        #rx.text(fe_list[2][0]),
        rx.foreach(
            fe_list.results,
            test_box
        ),
        border_width="1px",
        aspect_ratio="1/1",
        border_color="dimgray",
        direction="column"
    )

def test_box(results: FrontendResult) -> rx.Component:
    return rx.box(
        rx.text(
            results.acronym,
            font_size="xx-small",
        ),
        background_color=results.color
    ) 

def legend() -> rx.Component:
    return rx.flex(
        rx.text("Legend", 
                weight="bold",
                padding_bottom="10px"),
        rx.text("Abbreviations of the considered tests:"),
        rx.flex(
            rx.text(
                rx.text.strong("BAI"),
                " = Bad Analog Integrated"
            ),
            rx.text(
                rx.text.strong("TFF"),
                " = Threshold Failed Fits Independent"
            ),
            rx.text(
                rx.text.strong("HEN"),
                " = High ENC Independent"
            ),
            padding_left="20px",
            direction="column"
        ), 
        style=content_box_style,
        direction="column"
    )

def histogram_view() -> rx.Component:
    return rx.flex(
        rx.grid(
            rx.foreach(
                ScanAnalysisResultsState.summary_histograms, 
                histogram_box),
            rx.text(""),
            columns="2",
            spacing_y="5",
            spacing_x="5",
        ),
        background_color=rx.color("blue", 4),
        border_radius="10px",
    )

def histogram_box(hist_info: SummaryHistogram) -> rx.Component:
    return rx.box(
        rx.text(hist_info.name),
        rx.recharts.scatter_chart(
            rx.recharts.scatter(
                data=hist_info.data,
                fill="#8884d8",
            ),
            rx.recharts.scatter(
                data=hist_info.data,
                fill="#8884d8",
            ),
            rx.recharts.reference_line(
                y=hist_info.upper,
                stroke=rx.color("red", 8),
                if_overflow="extendDomain",
                #label=hist_info.upper,
            ),
            rx.recharts.reference_line(
                y=hist_info.lower,
                stroke=rx.color("green", 8),
                if_overflow="extendDomain",
            ),
            rx.recharts.graphing_tooltip(is_animation_active=False),
            rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False,
                label={
                    "value": hist_info.x_label,
                    "position": "center",
                    "dy": 14,
                }                  
            ),
            rx.recharts.y_axis(data_key="y", label={
                    "value": hist_info.y_label,
                    "angle": -90,
                    "position": "center",
                    "dx": -22
                },
            ),
            rx.recharts.z_axis(data_key="FE"),
            width="95%",
            height=300,
        ),
    )
