from pydantic import BaseModel
from typing import Optional
from enum import Enum
from abc import ABC
from typing import Optional
from pathlib import Path
from analysis_manager.PDB_models import (
    PDBMinimumHealthTestFrontEndResults,
    PDBScanTestProperties,
    PDBMinimumHealthTestFrontEndReport,
    pfa_pdb_report,
    module_e_summary
)


class TestTypes(str, Enum):
    MininumHealthTest = "Minimum Health Test"
    PixelFailureTest = "Pixel Failure Analysis"
    DigitalOnlyTest = "Digital Only Test"

class TestVariable(BaseModel):
    name: str
    value: int
    description: str = ""
    
class FrontendResult(BaseModel):
    color: str = "gainsboro"  # frontend color
    acronym: str = "BAI"  # frontend akronym
    variables: list[TestVariable] = []
    
class Frontend(BaseModel):
    serialnumber: str
    uuid: Optional[str] = None
    position: Optional[int] = None
    testtype: Optional[str] = None
    testresult: bool = False
    mht_fe_report: PDBMinimumHealthTestFrontEndReport
    mht_fe_results: PDBMinimumHealthTestFrontEndResults
    pfa_report: dict
    results: list[FrontendResult] = []

# TODO: constant info gets duplicated for each frontend, might be something to look into at some point

class ScanInfo(BaseModel):
    name: str = ""
    db_prefix: str = ""
    path_on_disk: Path = None
    active: bool = False 
    status: int = 1
    files: list[str] = []
    short: str = ""

class ModuleInfo(BaseModel):
    position: int
    module_serial: str
    module_uuid: str
    checked_for_analysis: bool
    scans_dict: dict[str, ScanInfo] = {"digitalscan": ScanInfo(status=0), "analogscan": ScanInfo(status=0)}
    label_color: str
    QC_result: bool
    serialnumber_color: str 
    e_summary: dict
    frontends: list[Frontend] = []

class FrontendID(BaseModel):
    uuid: str
    serial: str

class Module(BaseModel):
    uuid: str
    serial_number: str
    mod_pos_on_lls: int
    frontends: list[FrontendID]

class AnalysisParams(BaseModel):
    module: Module
    analysis_type: str
    analysis_uuid: str
    analysis_tag: str
    scan_tag: str
    write_results_to_db: bool

class ScanList():
    digitalscan: ScanInfo 
    analogscan: ScanInfo 
    thresholdscan_hr: ScanInfo 
    thresholdscan_hd: ScanInfo 
    noisescan: ScanInfo 
    totscan: ScanInfo 
    thresholdscan_zerobias: ScanInfo 
    discbumpscan: ScanInfo 
    mergedbumpscan: ScanInfo 
    sourcescan: ScanInfo 

    def __init__(self):
        self.digitalscan = ScanInfo(name="digitalscan", db_prefix="ds_", files=["ds_occupancymap"], short="d")
        self.analogscan = ScanInfo(name="analogscan", db_prefix="as_", files=["as_occupancymap", "as_meantotmap"], short="a")
        self.thresholdscan_hr = ScanInfo(name="thresholdscan_hr", db_prefix="hr_", files=["hr_chi2map", "hr_noisemap", "hr_thresholdmap", "hr_config"], short="t")
        self.thresholdscan_hd = ScanInfo(name="thresholdscan_hd", db_prefix="hd_", files=["hd_chi2map", "hd_noisemap", "hd_thresholdmap", "hd_config"], short="t")
        self.noisescan = ScanInfo(name="noisescan", db_prefix="ns_", files=["ns_noisemap", "ns_config"], short="n")
        self.totscan = ScanInfo(name="totscan", db_prefix="ts_", files=[])
        self.thresholdscan_zerobias = ScanInfo(name="thresholdscan_zerobias", db_prefix="tszb_", files=["tszb_chi2map", "tszb_noisemap", "tszb_thresholdmap", "tszb_config"], short="z")
        self.discbumpscan = ScanInfo(name="discbumpscan", db_prefix="db_", files=["db_occupancymap", "db_config"], short="b")
        self.mergedbumpscan = ScanInfo(name="mergedbumpscan", db_prefix="mbs_", files=["mbs_occupancymap", "mbs_config"], short="m")
        self.sourcescan = ScanInfo(name="sourcescan", db_prefix="sos_", files=["sos_occupancy", "sos_config"], short="s")

class AnalysisInfoBase(ABC):
    full_test_name: str
    workdir_prefix: str
    time_stamp_filename: str
    scan_list: ScanList 
    module_qc_test_name: str
    test_executable_name: str

    def get_scan_info_array(self) -> list[ScanInfo]:
        scan_info_array = []
        for scan in vars(self.scan_list).values():
            if isinstance(scan, ScanInfo) and scan.active:
                scan_info_array.append(scan)
        return scan_info_array
    
    def get_scan_info_dict(self) -> dict[str, ScanInfo]:
        return {scan.name: scan for scan in self.get_scan_info_array()}

class MinimumHealthTestInfo(AnalysisInfoBase):
    full_test_name = TestTypes.MininumHealthTest
    workdir_prefix = "mht"
    time_stamp_filename = "Time_stamps_execution_minimum_health_test.txt"
    module_qc_test_name = "MIN_HEALTH_TEST"
    test_executable_name = "analysis-MIN-HEALTH-TEST"
    scan_list = ScanList()

    def __init__(self):
        self.scan_list = ScanList()
        self.scan_list.digitalscan.active = True
        self.scan_list.analogscan.active = True
        self.scan_list.thresholdscan_hr.active = True

class PixelFailureAnalysisInfo(AnalysisInfoBase):
    full_test_name = TestTypes.PixelFailureTest
    workdir_prefix = "pfa"
    time_stamp_filename = "Time_stamps_execution_pixel_failure_test.txt"
    module_qc_test_name = "PIXEL_FAILURE_ANALYSIS"
    test_executable_name = "analysis-PIXEL-FAILURE-ANALYSIS"

    def __init__(self):
        self.scan_list = ScanList()
        self.scan_list.digitalscan.active = True
        self.scan_list.analogscan.active = True
        self.scan_list.thresholdscan_hd.active = True
        self.scan_list.noisescan.active = True
        self.scan_list.discbumpscan.active = True
        self.scan_list.mergedbumpscan.active = True
        self.scan_list.sourcescan.active = True
        self.scan_list.thresholdscan_zerobias.active = True

class DigitalTestInfo(AnalysisInfoBase):
    full_test_name = TestTypes.DigitalOnlyTest
    workdir_prefix = "dsa"
    time_stamp_filename = "Time_stamps_execution_digital_analysis_only.txt"
    module_qc_test_name = "MIN_HEALTH_TEST"

    def __init__(self):
        self.scan_list = ScanList()
        self.scan_list.digitalscan.active = True

class SummaryHistogram(BaseModel):
    name: str 
    data: list[dict]
    upper: int
    lower: int
    y_label: str
    x_label: str = "frontend number"

test_type_dict = {
    TestTypes.MininumHealthTest: MinimumHealthTestInfo,
    TestTypes.PixelFailureTest: PixelFailureAnalysisInfo,
    TestTypes.DigitalOnlyTest: DigitalTestInfo,
}

# Definition of scan data status flags
scan_data_status_flags = {
    "UNDEFINED" : 0,
    "GOOD" : 1
}