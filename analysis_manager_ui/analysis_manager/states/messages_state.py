import reflex as rx
import logging
from datetime import datetime

log_ms = logging.getLogger("Analysis_Manager_Logger")

class MessagesState(rx.State):
    # Substate to coordinate messages to be displayed in the messages panel.
    messages: list[str]
    max_messages: str = "20"

    def add_message(self, message: str):
        # Get limit of the number of messages to be displayed:
        max_limit = int(self.max_messages)
        log_ms.debug(f"maximum number of messages = {max_limit}")
        if (len(self.messages) >= max_limit):
            # print(f"add_message(): maximum number of messages = {max_limit} exceeded.")
            n_to_remove = len(self.messages)-max_limit+1
            for i in range(0, n_to_remove):
                # remove the oldest message (last message in the list)
                self.messages.pop()
        time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        self.messages.insert(0, time_stamp + ": " + message + "\n")
    
