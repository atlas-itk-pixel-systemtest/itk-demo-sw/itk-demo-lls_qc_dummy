# The backend of the analysis_manager app
# is based on the AnalysisManagerState class defined in this file.
#
import reflex as rx
import json
import time
import copy
from datetime import datetime
from pyconfigdb import ConfigDB
from pyregistry import ServiceRegistry
from pyconfigdb.exceptions import ConfigDBConnectionError as CCE
from uuid import uuid4
import logging
import httpx
from analysis_manager.states.messages_state import MessagesState
from analysis_manager.states.lls_state import LLSState
from analysis_manager.tools import qc_container_health_test, create_tag
from analysis_manager.models import (
    AnalysisParams,
    Module,
    scan_data_status_flags,
    FrontendID,
    test_type_dict,
    AnalysisInfoBase,
)
from analysis_manager.config import config
# from qc_models import AnalysisParams, Module

# Logger cannot be a member of the reflex state class
logging.basicConfig(
    format="%(asctime)s %(levelname)s %(name)s: %(message)s", level=logging.INFO
)
log = logging.getLogger("Analysis_Manager_Logger")


# Define scan data needed for scan analyses:


class AnalysisManagerState(rx.State):
    """The app state."""

    selected_lls_payload_id: str
    main_panel_type: str = "analysis_composition"
    tab_colors: list[str] = ["gainsboro", "silver", "silver", "silver"]
    select_scan_placeholder: str = "Select a scan tag"
    n_selected_mods: int = 0
    analysis_tag: str = "default_tag"
    submit_loading_state: bool = False
    select_all_mods_state = "select"
    scan_tag_check: bool = True
    scan_tag: str = ""
    show_composer_legend: bool = False
    selected_analysis_type: str = "Minimum Health Test"
    analysis_tag_list_placeholder: str = "Select an analysis tag"

    async def get_config_db(self) -> ConfigDB:
        ms: MessagesState = await self.get_state(MessagesState)
        try:
            db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
            return db
        except CCE:
            ms.add_message(
                "ERROR: Database is NOT accessible (health test failed)! Switching back to file access."
            )
            return None

    async def db_health_test(self):
        ms: MessagesState = await self.get_state(MessagesState)
        if config.use_db:
            db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
            db_health_result = db.health()
            if db_health_result == False:
                ms.add_message(
                    "ERROR: Database is NOT accessible (health test failed)! Switching back to file access."
                )
                # self.use_db = False
            else:
                log.debug("Health test of LLS config data base was successful!")

    @rx.event
    async def select_all_mods(self):
        # Toggle the state of the select_all_mods_state between
        # "select" and "deselect"
        llsstate: LLSState = await self.get_state(LLSState)
        ms: MessagesState = await self.get_state(MessagesState)
        if self.select_all_mods_state == "select":
            # Change the state to deselect:
            self.select_all_mods_state = "deselect"
            for m in llsstate.mod_info:
                # Perform the selection only if digital or analog scan data are available
                if (
                    m.scans_dict["digitalscan"].status == scan_data_status_flags["GOOD"]
                ) or (
                    m.scans_dict["analogscan"].status == scan_data_status_flags["GOOD"]
                ):
                    m.checked_for_analysis = True
                    self.n_selected_mods += 1
            ms.add_message(f"Selected {self.n_selected_mods} modules for analysis.")
        else:
            # Change the state to select:
            self.select_all_mods_state = "select"
            for m in llsstate.mod_info:
                if m.checked_for_analysis:
                    m.checked_for_analysis = False
                    self.n_selected_mods -= 1
            log.debug("All modules de-selected for analysis.")

    def toggle_composer_legend(self):
        self.show_composer_legend = not self.show_composer_legend

    @rx.event
    async def toggle(self, lls_pos: int):
        llsstate: LLSState = await self.get_state(LLSState)
        llsstate.mod_info[lls_pos - 1].checked_for_analysis = not llsstate.mod_info[
            lls_pos - 1
        ].checked_for_analysis
        self.n_selected_mods = len(
            [d for d in llsstate.mod_info if (d.checked_for_analysis == True)]
        )
        log.debug(f"Changed the number of selected modules to {self.n_selected_mods}")

    def check_scan_tag_information(
        self, st_info: dict, required_payloads: list[str], fe_id: str
    ):
        if fe_id in st_info:
            list_of_scan_tag_payloads = st_info[fe_id]
            for r in required_payloads:
                if not r in list_of_scan_tag_payloads:
                    return False
            # All requirements met
            return True
        else:
            # No scan data available for the front-end
            return False

    @rx.event
    async def load_scan_info(self, selected_scan_tag: str):
        # Set selected scan tag if input is valid:
        llsstate: LLSState = await self.get_state(LLSState)
        if selected_scan_tag not in llsstate.list_scan_tags:
            self.scan_tag_check = False
        else:
            self.scan_tag = selected_scan_tag
            self.scan_tag_check = True

            if llsstate.loaded_LLS_ID != llsstate.selected_LLS_ID:
                # Reset the number of selected modules
                self.n_selected_mods = 0

        # Set selected scan tag:
        self.scan_tag = selected_scan_tag
        ms: MessagesState = await self.get_state(MessagesState)
        ms.add_message(f"Load scan info for tag {self.scan_tag} for all modules.")
        # Get scan tag information from the data base:
        db = await self.get_config_db()
        st_info = db.search_in_tag(
            self.scan_tag, payload_types=[], order_by_object=True
        )

        analysis: AnalysisInfoBase = test_type_dict[self.selected_analysis_type]()
        active_scans = analysis.get_scan_info_dict()

        for m in llsstate.mod_info:
            m.scans_dict = copy.deepcopy(active_scans)
            # print(m)
            log.debug(f"Loading scan information for module at position {m.position}.")

            # Mapping between tests and the indices in the module info array:
            for fe in m.frontends:
                for scan in m.scans_dict.values():
                    if not self.check_scan_tag_information(
                        st_info, scan.files, fe.uuid
                    ):
                        scan.status = 4

        # Loop over modules finished.
        log.debug(f"Module info array:\n{llsstate.mod_info}")

    @rx.event
    async def set_analysis_type_and_reload_scan_tag_info(self, selected_analysis_type):
        self.selected_analysis_type = selected_analysis_type
        await self.load_scan_info(self.scan_tag)

    async def submit_analysis(self):
        llsstate: LLSState = await self.get_state(LLSState)
        ms: MessagesState = await self.get_state(MessagesState)
        self.submit_loading_state = True
        # Trigger a state update of the frontend to show the spinner:
        yield
        # Produce a list of the selected modules:
        mods_selected = [
            module for module in llsstate.mod_info if module.checked_for_analysis
        ]
        n_mods_submission = len(mods_selected)
        if (n_mods_submission) > 0:
            time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M%S")
            # List of all request payloads:
            run_analysis_list = []
            # List of data to be submitted to the data base:
            db_data = []
            # Create an minimum health test uuid:
            ana_uuid = uuid4().hex
            # Prepare a message string:
            submission_message_string = (
                f"submit_analysis(): Preparing {self.selected_analysis_type} "
            )
            if n_mods_submission == 1:
                submission_message_string += "request for the module at the position "
            else:
                submission_message_string += (
                    "requests for the modules at the positions "
                )
            # Loop over selected modules for which an analysis is requested:
            for m in mods_selected:
                if n_mods_submission == 1:
                    submission_message_string += f"{m.position}."
                else:
                    submission_message_string += f"{m.position}, "
                # Prepare request payload:

                if self.analysis_tag in llsstate.list_analysis_tags:
                    new_analysis_tag = self.analysis_tag
                else:
                    # Add lls_tag to anaylsis tag, if new tag
                    new_analysis_tag = f"{llsstate.selected_LLS_ID}_{self.analysis_tag}"

                module = Module(
                    uuid=m.module_uuid,
                    serial_number=m.module_serial,
                    mod_pos_on_lls=m.position,
                    frontends=[
                        FrontendID(uuid=fe.uuid, serial=fe.serialnumber)
                        for fe in m.frontends
                    ],
                )

                ana_payload = AnalysisParams(
                    module=module,
                    analysis_type=self.selected_analysis_type,
                    analysis_uuid=ana_uuid,
                    analysis_tag=new_analysis_tag,
                    scan_tag=self.scan_tag,
                    write_results_to_db=config.use_db,
                )
                ana_payload_dict = ana_payload.model_dump()
                run_analysis_list.append(ana_payload_dict)
                # Prepare entry for db data:
                db_data.append(
                    {
                        "type": f"{self.selected_analysis_type} payload",
                        "data": json.dumps(ana_payload_dict),
                    }
                )
            #
            # ms.add_message(submission_message_string)
            # Write to data base:
            lls_db = await self.get_config_db()
            if lls_db:
                # Write the analyis payloads to the LLS data base:
                db_data.append(
                    {
                        "meta": True,
                        "type": "metadata",
                        "data": json.dumps({"timestamp": time_stamp}),
                    }
                )

                # Create an entry in the tag table of the data base:
                meta_tag = f"{llsstate.selected_LLS_ID}_analysis_tags"
                create_tag(
                    lls_db=lls_db, name=meta_tag, type="analysis_tags", stage=False
                )
                create_tag(
                    lls_db=lls_db,
                    name=new_analysis_tag,
                    type="analysis_tag",
                    stage=False,
                    groups=[meta_tag],
                )
                lls_db.add_node(
                    id=ana_uuid,
                    type=self.selected_analysis_type,
                    payloads=db_data,
                    stage=False,
                    tags=[new_analysis_tag],
                )

            # Submit http requests:
            i_sub = 0
            registry = ServiceRegistry(srUrl=config.sr_url)
            qc_api_url = registry.get(key=config.qc_api_key)
            log.info(f"QC API URL: {qc_api_url}")
            if qc_container_health_test(f"{qc_api_url}/health") == True:
                # async with httpx.AsyncClient(follow_redirects=True) as client:
                for min_health_payload in run_analysis_list:
                    i_sub += 1
                    ms.add_message(f"Request analysis with: {min_health_payload}")
                    yield
                    # url = "http://wupp-charon.cern.ch:5112/minimum_health/?"
                    url = f"{qc_api_url}/minimum_health/?"
                    headers = {
                        "accept": "application/json",
                        "Content-Type": "application/json",
                    }
                    # response = await client.post(url, json=min_health_payload, headers=headers)
                    response = httpx.post(
                        url,
                        json=min_health_payload,
                        headers=headers,
                        follow_redirects=True,
                        timeout=None,
                    )
                    if response.status_code == 202:
                        ms.add_message(f"Request succeeded. Response: {response.text}")
                    # Sleep a short time to make the http requests come with some time delay
                    # with respect to each other:
                    time.sleep(0.20)
            else:
                log.error(
                    "submit_analysis(): QC container is not available! Health test failed."
                )
                ms.add_message(
                    "submit_analysis(): QC container is not available! Health test failed."
                )
        else:
            log.warning("submit_analysis(): There are no selected modules.")
        self.submit_loading_state = False

    @rx.event
    async def change_main_panel(self, panel_name: str):
        llsstate: LLSState = await self.get_state(LLSState)
        self.main_panel_type = panel_name
        match panel_name:
            case "analysis_composition":
                self.tab_colors[0] = "gainsboro"
                self.tab_colors[1] = "silver"
                self.tab_colors[2] = "silver"
                self.tab_colors[3] = "silver"
            case "results_viewer":
                self.tab_colors[0] = "silver"
                self.tab_colors[1] = "gainsboro"
                self.tab_colors[2] = "silver"
                self.tab_colors[3] = "silver"
                # Access the Config DB and obtain a list of analysis tags
                if config.use_db == True:
                    # Reload the LLS information (runkey) because in the meanwhile
                    # an analysis may have been performed and a new analysis tag was created
                    await llsstate.load_analysis_tags()
            case "electrical_tests":
                self.tab_colors[0] = "silver"
                self.tab_colors[1] = "silver"
                self.tab_colors[2] = "gainsboro"
                self.tab_colors[3] = "silver"
            case "stage_sign_off":
                self.tab_colors[0] = "silver"
                self.tab_colors[1] = "silver"
                self.tab_colors[2] = "silver"
                self.tab_colors[3] = "gainsboro"
